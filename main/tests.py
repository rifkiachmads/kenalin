from django.test import TestCase
from django.test import Client
from django.urls import resolve
from .views import index, login_page
from main.models import User

import environ
import os
root = environ.Path(__file__) - 3 # three folder back (/a/b/c/ - 3 = /)
env = environ.Env(DEBUG=(bool, False),)
environ.Env.read_env('.env')

# Create your tests here.
class MainUnitTest(TestCase):
    def setUp(self):
        self.username = env("SSO_USERNAME")
        self.password = env("SSO_PASSWORD")
       # self.login = self.client.post('/auth/login/', {'username': self.username, 'password': self.password})

    def test_login_failed(self):
    	response = self.client.post('/auth/login/', {'username': "siapa", 'password': "saya"})
    	self.assertRedirects(response, '/auth/sso/?', 302, 200)
    	self.assertTemplateUsed('main/login.html')

    def test_logout(self):
    	response = self.client.post('/auth/login/', {'username': self.username, 'password': self.password})
    	self.assertEqual(response.status_code, 302)
    	response = self.client.post('/auth/logout/')
    	self.assertEqual(response.status_code, 302)

    def test_landing_page(self):
    	response = self.client.post('/auth/')
    	self.assertTemplateUsed('main/landing_page.html')