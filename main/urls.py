from django.conf.urls import url
from .custom_auth import auth_login, auth_logout
from .views import index, login_page
urlpatterns = [
    url(r'^$', index, name='index'),
    url(r'^sso/$', login_page, name='sso'),
    url(r'^login/$', auth_login, name='login'),
    url(r'^logout/$', auth_logout, name='logout'),
]
