from django.shortcuts import render

res = {}
# Create your views here.
def index(request):
	html = 'main/landing_page.html'
	return render(request, html, res)

def login_page(request):
	html = 'main/login.html'
	return render(request, html, res)