# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import migrations, models
import django.db.models.deletion


class Migration(migrations.Migration):

    initial = True

    dependencies = [
    ]

    operations = [
        migrations.CreateModel(
            name='User',
            fields=[
                ('username', models.CharField(max_length=20, primary_key=True, serialize=False, unique=True)),
                ('npm', models.CharField(max_length=20, unique=True)),
                ('profile_pic', models.URLField(default='https://www.lausanne.org/wp-content/uploads/2017/04/anonymous-icon-150x150.jpg')),
                ('flag_nilai', models.BooleanField(default=False)),
            ],
        ),
    ]
