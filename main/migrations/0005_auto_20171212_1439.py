# -*- coding: utf-8 -*-
# Generated by Django 1.11.4 on 2017-12-12 07:39
from __future__ import unicode_literals

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('main', '0004_auto_20171212_1432'),
    ]

    operations = [
        migrations.AlterField(
            model_name='expertise',
            name='level',
            field=models.CharField(choices=[('1', 'Beginner'), ('2', 'Intermediate'), ('3', 'Advanced'), ('4', 'Expert')], default='1', max_length=20),
        ),
    ]
