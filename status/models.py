from django.db import models
from main.models import User

# Create your models here.
class Status(models.Model):
    username = models.ForeignKey(User, on_delete=models.CASCADE)
    status = models.TextField(max_length=140)
    created_date = models.DateTimeField(auto_now_add=True)