# -*- coding: utf-8 -*-
from __future__ import unicode_literals

import json

from django.contrib import messages
from django.http import HttpResponseRedirect, HttpResponse
from django.shortcuts import render
from django.urls import reverse
from django.views.decorators.csrf import csrf_exempt
from django.http import JsonResponse
from main.models import User
from .models import Status
from .forms import Status_Form

response = {}

def index(request, username):
	response['login_name'] = request.session.get('user_login')
	if 'user_login' in request.session:
		name = request.session.get('user_login', None)
		print(username)
		print(name)
		response['show_form'] = False if name == username else True

	user = User.objects.get(username=username)
	status = Status.objects.filter(username=username).order_by('-id')
	response['image'] = user.profile_pic
	response['name'] = user.username
	response['status'] = status
	response['status_form'] = Status_Form		
	html = 'status/status.html'
	return render(request, html, response)

def add_status(request):
	form = Status_Form(request.POST or None)
	if(request.method == 'POST' and form.is_valid()):
		response['isi'] = request.POST['status']
		username = request.session['user_login']
		user = User.objects.get(username=username)
		status = Status(username=user,status=response['isi'])
		status.save()
	return HttpResponseRedirect('/status/'+username)

def delete_status(request):
	if (request.method == 'POST'):
		username = request.session['user_login']
		statusId = request.POST['status-id']
		status = Status.objects.get(id=statusId)
		status.delete()
		return HttpResponseRedirect('/status/'+username)
	elif (request.method == 'GET'):
		username = request.session['user_login']
		return HttpResponseRedirect('/status/'+username)
