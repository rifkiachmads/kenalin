from django.conf.urls import url
from .views import index, add_status, delete_status

urlpatterns = [
    url(r'^(?P<username>.*)/$', index, name='status'),
    url(r'^add_status', add_status, name='add_status'),
    url(r'^delete_status', delete_status, name='delete_status'),
]
