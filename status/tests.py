from django.test import TestCase
from django.test import Client
from django.urls import resolve
from .views import index, add_status, delete_status
from .models import Status
from main.models import User
from .forms import Status_Form

import environ
import os
root = environ.Path(__file__) - 3 # three folder back (/a/b/c/ - 3 = /)
env = environ.Env(DEBUG=(bool, False),)
environ.Env.read_env('.env')

# Create your tests here.
class StatusUnitTest(TestCase):
    def setUp(self):
        self.username = env("SSO_USERNAME")
        self.password = env("SSO_PASSWORD")
        self.login = self.client.post('/auth/login/', {'username': self.username, 'password': self.password})

    def test_status_url_is_exist(self):
        response = Client().get('/status/'+self.username+'/')
        self.assertEqual(response.status_code, 200)

    def test_status_page_when_user_is_logged_in(self):
        response = self.client.get('/status/'+self.username+'/')
        self.assertEqual(response.context['login_name'], self.username)

        html_response = response.content.decode('utf-8')
        self.assertEqual(response.status_code, 200)
        self.assertTemplateUsed('status/status.html')

    def test_status_post_success_and_render_the_result(self):
        test = 'Anonymous'
        response_post = self.client.post('/status/add_status', {'status': test})
        self.assertEqual(response_post.status_code, 302)

        response = self.client.get('/status/'+self.username+'/')
        html_response = response.content.decode('utf8')
        self.assertIn(test, html_response)

    def test_status_delete_status_and_render_the_result(self):
        message = "This will be deleted"
        user = User.objects.get(username=self.username)
        status = Status(status=message, username=user)
        status.save()
        response = self.client.post('/status/delete_status', {'status-id': status.id})
        html_response = response.content.decode('utf8')
        self.assertNotIn(message, html_response)

    def test_status_delete_status_redirect(self):
        response = self.client.get('/status/delete_status')
        self.assertRedirects(response, '/status/'+self.username, 302, 301)