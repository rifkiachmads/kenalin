"""kenalin URL Configuration

The `urlpatterns` list routes URLs to views. For more information please see:
    https://docs.djangoproject.com/en/1.11/topics/http/urls/
Examples:
Function views
    1. Add an import:  from my_app import views
    2. Add a URL to urlpatterns:  url(r'^$', views.home, name='home')
Class-based views
    1. Add an import:  from other_app.views import Home
    2. Add a URL to urlpatterns:  url(r'^$', Home.as_view(), name='home')
Including another URLconf
    1. Import the include() function: from django.conf.urls import url, include
    2. Add a URL to urlpatterns:  url(r'^blog/', include('blog.urls'))
"""
from django.conf.urls import url, include
from django.contrib import admin
from django.views.generic.base import RedirectView
import status.urls as status
import aboutme.urls as aboutme

urlpatterns = [
    url(r'^admin/', admin.site.urls),
    url(r'^aboutme/', include(aboutme, namespace='aboutme')),
    url(r'^academic/', include('academic.urls', namespace='academic')),
    url(r'^auth/', include('main.urls', namespace='main')),
    url(r'^status/', include(status, namespace='status')),
    url(r'^search/', include('search.urls', namespace='search')),
    url(r'^$', RedirectView.as_view(url='auth/', permanent='True')),
]
