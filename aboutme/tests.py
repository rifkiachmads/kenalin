from django.test import TestCase
from django.test import Client
from django.urls import resolve
from .views import index, edit, edit_profile, showGrades, addExpertise
from main.models import User, Expertise

import environ
import os

root = environ.Path(__file__) - 3  # three folder back (/a/b/c/ - 3 = /)
env = environ.Env(DEBUG=(bool, False), )
environ.Env.read_env('.env')


# Create your tests here.
class AboutMeUnitTest(TestCase):
    def setUp(self):
        self.username = env("SSO_USERNAME")
        self.password = env("SSO_PASSWORD")
        self.login = self.client.post('/auth/login/', {'username': self.username, 'password': self.password})

    def test_aboutme_url_is_exist(self):
        response = Client().get('/aboutme/' + self.username + '/')
        self.assertEqual(response.status_code, 200)

    def test_aboutme_using_index_func(self):
        found = resolve('/aboutme/' + self.username + '/')
        self.assertEqual(found.func, index)

