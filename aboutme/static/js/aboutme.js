// Setup an event listener to make an API call once auth is complete
function onLinkedInLoad() {
    IN.Event.on(IN, "auth", getProfileData);
}

// Use the API call wrapper to request the member's profile data
function getProfileData() {
    IN.API.Profile("me").fields("id", "first-name", "last-name", "headline", "location", "picture-url", "public-profile-url", "email-address").result(displayProfileData).error(onError);
}

// Handle the successful return from the API call
function displayProfileData(data) {
    console.log(data);
    var user = data.values[0];
    document.getElementById("profilepic").outerHTML = '<img id="profilepic" src="' + user.pictureUrl + '" height=100 width= 100 class="img-circle">';
    $("#name").append('<b>Nama : </b> ' + user.firstName + ' ' + user.lastName);
    $("#email").append('<b>Email : </b>' + user.emailAddress);
    $("#linkedInProfile").append('<b>Profil LinkedIn : </b>' + user.publicProfileUrl);
    document.getElementById('profileData').style.display = 'block';
    // $.ajax({
    //     method: "POST",
    //     url: '/about/me',
    //     data: {
    //         name: user.firstName + " " + user.lastName,
    //         email: user.emailAddress,
    //         linkedin_profile: user.publicProfileUrl
    //     },
    //     dataType: 'json',
    //     success: function (user) {
    //         document.getElementById("profilepic").outerHTML = '<img id="profilepic" src="' + user.pictureUrl + '" height=100 width= 100 class="img-circle">';
    //         $("#name").append('<b>Nama : </b> ' + user.firstName + ' ' + user.lastName);
    //         $("#email").append('<b>Email : </b>' + user.emailAddress);
    //         $("#linkedInProfile").append('<b>Profil LinkedIn : </b>' + user.publicProfileUrl);
    //         document.getElementById('profileData').style.display = 'block';
    //     },
    //     error: function (error) {
    //         alert("Aduh gatau kak saya udah pusing :(")
    //     }
    // });
}

// Handle an error response from the API call
function onError(error) {
    console.log(error);
}

// Destroy the session of linkedin
function logout() {
    IN.User.logout(removeProfileData);
}

// Remove profile data from page
function removeProfileData() {
    document.getElementById('profileData').remove();
}