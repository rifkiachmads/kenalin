from django.conf.urls import url
from .views import *

urlpatterns = [
    url(r'^add-expertise/(?P<expertise>.*)/(?P<level>.*)/$', addExpertise, name='addExpertise'),
    url(r'^edit/', edit, name='edit'),
    url(r'^edit-profile', edit_profile, name='edit-profile'),
    url(r'^show-grades', showGrades, name='show-grades'),
    url(r'^(?P<username>.*)/$', index, name='index'),
]
