# TUGAS 1 KELOMPOK 5

## ANGGOTA KELOMPOK
- Fathya Annasya Yuzrin
- Purwo Aji Fahmi Akmal
- Rifki Achmad Subagyo
- Swastinika Naima Moertadho

## STATUS APLIKASI
[![pipeline status](https://gitlab.com/rifkiachmads/kenalin/badges/master/pipeline.svg)](https://gitlab.com/rifkiachmads/kenalin/commits/master)

[![coverage report](https://gitlab.com/rifkiachmads/kenalin/badges/master/coverage.svg)](https://gitlab.com/rifkiachmads/kenalin/commits/master)

## LINK HEROKU APP
http://kenalinlagi.herokuapp.com
