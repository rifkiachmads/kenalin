from django.shortcuts import render
from main.models import User
from academic.api_riwayat import get_riwayat
from django.http import JsonResponse

import json

res = {}

def index(request):
    res['key'] = ''
    html = 'search/search.html'
    res['data'] = get_user_data()
    return render(request, html, res)

def search(request, key):
    res['key'] = key
    html = 'search/search.html'
    res['data'] = get_user_data()
    return render(request, html, res)

def get_user_data():
    users = User.objects.all()
    data = []
    for user in users:
        temp = {}
        temp['username'] = user.username
        temp['npm'] = user.npm
        temp['profile_pic'] = user.profile_pic
        temp['name'] = user.name
        temp['email'] = user.email
        temp['linkedin'] = user.linkedin_profile
        temp['expertise'] = []
        exps = user.expertise.all()
        for exp in exps:
            temp['expertise'].append([exp.level, exp.expertise])
        data.append(temp)
    return json.dumps(data)

def search_api(request, username):
    user = User.objects.filter(username=username).first()
    temp = {}
    temp['username'] = user.username
    temp['npm'] = user.npm
    temp['profile_pic'] = user.profile_pic
    temp['name'] = user.name
    temp['email'] = user.email
    temp['linkedin'] = user.linkedin_profile
    temp['expertise'] = []
    exps = user.expertise.all()
    for exp in exps:
        temp['expertise'].append([exp.level, exp.expertise])
    temp['riwayat'] = get_riwayat().json()
    

    return JsonResponse(temp)
