from django.test import TestCase
from django.test import Client
from main.models import User, Expertise

class searchUnitTest(TestCase):
    def test_search_url_is_exist(self):
        response = Client().get('/search/')
        self.assertEqual(response.status_code, 200)

    def test_search_url_with_parameter(self):
        user = User(username="test", npm="123")
        user.save()
        exp = Expertise(expertise="TDD with django uyeah")
        exp.save()
        user.expertise.add(exp)
        response = Client().get('/search/rifki/')
        self.assertEqual(response.status_code, 200)
