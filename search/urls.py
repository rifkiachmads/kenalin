from django.conf.urls import url
from .views import index, search, search_api

urlpatterns = [
    url(r'^$', index, name='index'),
    url(r'^api/(?P<username>.*)/$', search_api, name='search_api'),
    url(r'^(?P<key>.*)/$', search, name='search'),
]
