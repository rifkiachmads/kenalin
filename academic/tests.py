from django.test import TestCase
from django.test import Client
from django.urls import resolve
from .views import index
from .api_riwayat import get_riwayat
# Create your tests here.
import environ
import os
root = environ.Path(__file__) - 3 # three folder back (/a/b/c/ - 3 = /)
env = environ.Env(DEBUG=(bool, False),)
environ.Env.read_env('.env')

class academicUnitTest(TestCase):
	def setUp(self):
		self.username = os.environ.get("SSO_USERNAME")
		self.password = os.environ.get("SSO_PASSWORD")
		login = self.client.post('/auth/login/', {'username': self.username, 'password': self.password})

	def test_academic_url_is_exist(self):
		response = Client().get('/academic/'+self.username+'/')
		self.assertEqual(response.status_code, 200)

	def test_academic_using_index_func(self):
		found = resolve('/academic/'+self.username+'/')
		self.assertEqual(found.func, index)
