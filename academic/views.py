from django.shortcuts import render
from .api_riwayat import get_riwayat
from django.http import HttpResponseRedirect
from django.urls import reverse
from main.models import User
# Create your views here.

response = {}

def index(request, username):
	response['login_name'] = request.session.get('user_login')
	response['riwayat'] = get_riwayat().json()
	html = 'academic/riwayat.html'
	pengguna = User.objects.filter(username = username)
	response['flag'] = pengguna[0].flag_nilai
	return render(request, html, response)