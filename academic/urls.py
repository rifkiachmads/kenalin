from django.conf.urls import url
from .views import index

urlpatterns = [
	url(r'^(?P<username>.*)/$', index, name='index'),
]